public class LuggageLimitExceedException extends Exception {
    public LuggageLimitExceedException(String msg) {
        super(msg);
    }
}