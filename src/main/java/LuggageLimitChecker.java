public class LuggageLimitChecker {

    public static void checkLimit(Traveller traveller) throws LuggageLimitExceedException {
        if (traveller.getWeight() >= 16)
            throw new LuggageLimitExceedException("Weight exceeded the limit. " + traveller.getName() +
                    " has to pay the additional charges of " + (traveller.getWeight() - 15) * 500);
    }

    public static void checkLuggageWeight(Traveller traveller) {
        try {
            checkLimit(traveller);
            System.out.println("No additional charges are required for " + traveller.getName());
        } catch (LuggageLimitExceedException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Traveller Sruthi = new Traveller("Sruthi", 15.1);
        Traveller Bhavya = new Traveller("Bhavya", 18);
        checkLuggageWeight(Sruthi);
        checkLuggageWeight(Bhavya);
    }
}
